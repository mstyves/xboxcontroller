package driver;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;

import json.*;

import de.hardcode.jxinput.JXInputManager;
import de.hardcode.jxinput.directinput.DirectInputDevice;
import de.hardcode.jxinput.event.JXInputEventManager;

public class Manette {
	private ArrayList<DirectInputDevice> devices = new ArrayList<DirectInputDevice>();
	
	public String encode(){
		return "{\"B\":"+Arrays.toString(getButtonStates(0))+",\"P\":"+getDirection(0)+",\"X\":"+getAxis(0, 0)+",\"Y\":"+getAxis(0, 1)+"}";
	}
	
	public boolean disconnect(){
		 this.devices = new ArrayList<DirectInputDevice>();
		 return true;
	}
	
	public boolean connect(){
		JXInputEventManager.setTriggerIntervall( 50 );
	    for(int i = 0; i < JXInputManager.getNumberOfDevices(); i++){
	            if(JXInputManager.getJXInputDevice(i).getName().equals("Controller (XBOX 360 For Windows)")){
	                    this.devices.add( new DirectInputDevice(i));
	                    System.out.println("found! ");
	            }
	    }	
	    
	    return isAnyManetteConnected();
	}
	
	public boolean isAnyManetteConnected(){
		return this.devices.size() > 0;
	}
	
	public Integer numberOfManetteConnected(){
		return this.devices.size();
	}
	
	public Manette(){
		  
	}
	
	private ArrayList<Integer> getButtonsPressed(int device_id){
		if(devices.size() > 0){
			ArrayList<Integer> btns = new ArrayList<Integer>();
			DirectInputDevice selected_device = this.devices.get(device_id);
			
			// Manette Xbox a 10 bouttons
			for (int i = 0; i < 10; i++) {
				if(selected_device.getButton(i).getState())
				{
					btns.add(i);
				}
			}
			
			return btns;
		}
		else{
			return null;
		}
	}

	private int[] getButtonStates(int device_id){
		if(devices.size() > 0){
			int[] btns = {0,0,0,0,0,0,0,0,0,0};
			DirectInputDevice selected_device = this.devices.get(device_id);
			
			// Manette Xbox a 10 bouttons
			for (int i = 0; i < 10; i++) {
				btns[i] = (selected_device.getButton(i).getState()) ? 1 : 0 ;
			}
			
			return btns;
		}
		else{
			return null;
		}
	}
	
	private Integer getFirstButtonPressed(int device_id){
		if(devices.size() > 0){
			if(getButtonsPressed(device_id).size() > 0)
				return getButtonsPressed(device_id).get(0);
			else
				return null;
		}else{
			return null;
		}
	}
	
	private Double getAxis(int device_id, int axis){
		if(devices.size() > 0){
			if(axis >= 0 && axis <=1){
				DirectInputDevice selected_device = this.devices.get(device_id);
				return Math.round(selected_device.getAxis(axis).getValue()*10000)/10000.0;
			}
			return null;
		}else{
			return null;
		}
	}

	// en degree de 0 a 360'
	private Integer getDirection(int device_id){
		if(devices.size() > 0){
		DirectInputDevice selected_device = this.devices.get(device_id);
		
		if(selected_device.getDirectional(0).getDirection()> 0)
			return selected_device.getDirectional(0).getDirection()/100;
		else
			return 0;
		}else{
			return null;
		}
	}
}
